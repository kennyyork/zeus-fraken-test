var b = require('bonescript');
 
var switchPin = "P8_12";

// configure pins 
b.pinMode(switchPin, b.INPUT);
 
var tId = setInterval (inputHandler, 100);
 
function inputHandler() {     // detect switch push
   b.digitalRead(switchPin, function(x) {
       if( x.value === 0 ) {
           clearInterval(tId);
           console.log('Button\t\t[Success]');
           process.exit();
       }
   });
}
 
console.log('Press button to continue...');