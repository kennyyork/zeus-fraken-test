var b = require('bonescript');
 
var buzzerPin = "P8_13";

function done() {
    b.analogWrite(buzzerPin, 0);
    process.exit();
}

b.pinMode(buzzerPin, b.OUTPUT);
b.analogWrite(buzzerPin, b.LOW);
b.analogWrite(buzzerPin, 0.5);

setTimeout(done, 2000);

console.log('Buzzer should be beeping');