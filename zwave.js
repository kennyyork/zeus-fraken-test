var OZW = require('openzwave');
var zwave = new OZW('/dev/ttyO1');

var timeoutId = setTimeout(failOut, 10000);

function done(status) {
    console.log('ZWave:\t\t[%s]', status);
    clearTimeout(timeoutId);
    process.exit();
}

zwave.on('driver ready', function(homeid) {
    //console.log('scanning homeid=0x%s...', homeid.toString(16));
});

zwave.on('scan complete', function() {
    done('Success');
});

function failOut() {
    done('Failed');
}

zwave.connect();