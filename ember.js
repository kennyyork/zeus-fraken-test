var serialport = require('serialport');
var b = require('bonescript');

var timeoutId = setTimeout(failOut, 10000);

function done(status) {
    console.log('Zigbee:\t\t[%s]', status);
    clearTimeout(timeoutId);
    process.exit();
}

function failOut() {
    done('Failed');
}

var sp = new serialport.SerialPort('/dev/ttyO2', {
    baudrate: 115200,
    parser : serialport.parsers.readline('\n')
});

sp.on('open', function() {
    sp.write('\n\ntest\n');
    sp.on('data', function(data) {
        done('Success');
        sp.close();
    });
});