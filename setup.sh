#!/bin/bash

reboot=""

if [ ! -e "/dev/ttyO4" ]; then
    sudo apt-get install libudev-dev
    echo "Configuring UARTs..."
    echo "optargs=quiet drm.debug=7 capemgr.enable_partno=BB-UART1,BB-UART2,BB-UART4" >> /boot/uboot/uEnv.txt
    reboot="yes"
else
    echo "UARTs already configured."
    #npm install -g openzwave
    #npm install -g serialport
fi

if [ -n "$reboot" ]; then
    echo "Rebooting..."
    sudo shutdown -r now
else
    echo "Running tests..."
    node ember.js
    node zwave.js
    node buzzer.js
    node button.js
fi